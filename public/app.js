var myApp = angular.module('app', ['ui.router', 'satellizer', 'ngTable', 'chart.js'])

myApp.config(function ($stateProvider, $authProvider, $httpProvider) {

  var login = {
    name: 'login',
    url: '/login',
    component: 'login'
  }

  var homeState = {
    name: 'home',
    url: '/',
    component : 'home'
  }

  var asignementState = {
    name: 'asignements',
    url: '/asignements',
    component : 'asignements'
  }

  var chartState = {
    name: 'chart',
    url: '/chart',
    component: 'chartComp'
  }

  var rolesState = {
    name: 'roles',
    url: '/roles',
    component: 'roles'
  }

  $stateProvider.state(login)
  $stateProvider.state(homeState)
  $stateProvider.state(asignementState)
  $stateProvider.state(chartState)
  $stateProvider.state(rolesState)

  // $authProvider.loginUrl = 'http://localhost:3000/v1/user/login/'
  // $authProvider.signupUrl = 'http://localhost:3045/v1/user'
  $authProvider.tokenName = 'x-auth'
  $authProvider.tokenPrefix = ''

  // $httpProvider.interceptors.push('sessionInjector')

  $authProvider.google({
    clientId: '672147777430-6cpu222etmm196pclav4p54emq93bnim.apps.googleusercontent.com',
    url: 'http://localhost:3000/v1/user/login/google'

  })

  $authProvider.twitter({
    url: 'http://localhost:3000/v1/user/login/twitter'
  })

  $authProvider.facebook({
    clientId: '1931415310438591',
    url: 'http://localhost:3000/v1/user/login/facebook'

  })

  $authProvider.live({
    clientId : '9d5dafa1-c9c6-42d2-ad74-204ba1a9c9bb',
    url: "http://localhost:3000/v1/user/login/windowslive"
  });

}).run(runConfiguration)

var runConfiguration = function ($rootScope, $auth, $location) {
  $rootScope.$on('$routeChangeStart', function (event) {
    if (!$auth.isAuthenticated()) {
      event.preventDefault()
      $location.path('/login')
    }
  })


  $rootScope.currentUser = '';


}


