var module = angular.module('app')

var getAsignements = function ($http) {
  var req = {
    method: 'GET',
    url: '../sellers.json'
  }
  return $http(req).then(function (data) {
    return data.data
  }).catch(function (err) {
    return err
  })
}

var controller = function ($http, NgTableParams) {
  var self = this
  self.asignements = []


  self.$onInit = function () {
    getAsignements($http).then(function (allAsignements) {
      console.log(allAsignements)
      self.tableParams = new NgTableParams({}, {dataset : allAsignements})
    })
  }

}

module.component('tableAsigments', {
  templateUrl: '../templates/tableAsigments.html',
  controllerAs: 'self',
  controller: ['$http', 'NgTableParams', controller]
})