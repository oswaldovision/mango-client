var module = angular.module('app');

var controller = function($auth,$location, $rootScope, AuthService) {
  var ctlModel = this;

  ctlModel.credentials = {
    email : '',
    password : ''
  }

  ctlModel.authenticateProvider = function (provider) {
    console.log(provider)
    $auth.authenticate(provider).then(function (res) {
      console.log(res.data.email);
      $rootScope.currentUser = res.data.email;
      $auth.setToken(res.headers('x-auth'))
      $location.path('/login');
    }).catch(function (err) {
      console.log(err);

    })
  }

  ctlModel.authenticate = function (credentials) {
    AuthService.login(credentials).then(function (user) {
      // $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
      // $scope.setCurrentUser(user);

    }, function (err) {
      // $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
      console.log('fail login ' + err)
    });
  }

  ctlModel.logout = function () {
    AuthService.logout();
  }
}

module.component('login', {
  templateUrl: "../templates/login.html",
  controllerAs : "ctlModel",
  controller : [ '$auth','$location','$rootScope','AuthService',controller]
})